CONCEPTS_MAP = {
    'RP': 'RESEARCH_PROBLEM',
    'P': 'PROCESS',
    'METH': 'METHOD',
    'R': 'RESOURCE',
    'S': 'SOLUTION',
    'LOC': 'LOCATION',
    'T': 'TECHNOLOGY'
}
