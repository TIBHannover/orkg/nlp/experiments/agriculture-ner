from argparse import ArgumentParser

from src.models import predict


def parse_args():
    parser = ArgumentParser()

    parser.add_argument('-t', '--task',
                        choices=['predict'],
                        required=True,
                        help='Indicates the task to be executed. '
                             'predict: predicts the provided model using the provided query.'
                        )

    parser.add_argument('-mp', '--model_path',
                        type=str,
                        required=True,
                        help='Path to the model. '
                        )

    parser.add_argument('-q', '--query',
                        type=str,
                        required=True,
                        help='A paper\'s title. Used in the prediction setting.'
                        )

    return parser.parse_args()


def main():
    args = parse_args()

    {
        'predict': predict.main
    }[args.task](args)


if __name__ == '__main__':
    main()
