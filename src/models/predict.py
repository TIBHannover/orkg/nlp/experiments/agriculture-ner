from argparse import ArgumentParser

from transformers import pipeline

from src import CONCEPTS_MAP


def parse_args():
    parser = ArgumentParser()

    parser.add_argument('-mp', '--model_path',
                        type=str,
                        required=True,
                        help='Path to the model. '
                        )

    parser.add_argument('-q', '--query',
                        type=str,
                        required=True,
                        help='A paper\'s title. Used in the prediction setting.'
                        )

    return parser.parse_args()


def main(config=None):
    args = config or parse_args()

    assert args.model_path, 'model_path must be provided.'
    assert args.query, 'query must be provided.'

    model = pipeline(
        task='token-classification',
        model=args.model_path,
        tokenizer='bert-base-cased',
        aggregation_strategy='simple'
    )
    output = model(args.query)

    annotations = []
    seen = []
    for entity in output:

        if entity['entity_group'] in seen:
            continue

        seen.append(entity['entity_group'])
        annotations.append({
            'concept': CONCEPTS_MAP[entity['entity_group']],
            'entities': [i['word'] for i in output if i['entity_group'] == entity['entity_group']]
        })

    print(annotations)
    return annotations


if __name__ == '__main__':
    main()
