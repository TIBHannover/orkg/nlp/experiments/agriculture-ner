# ORKG Agriculture NER

## Overview

### Aims
The Agri-NER service aims at fostering the ORKG construction by recommending concepts to the user from their given
paper's title that can be considered as predicates being added to the graph. It is designed for contribution-centric
scientific entity extraction and classification from paper titles. The entity core concepts were inspired in part
from the [AGROVOC ontology](https://agrovoc.fao.org/browse/agrovoc/en/) and are:

| **Text** |                                       **Concepts**                                        |
|:--------:|:-----------------------------------------------------------------------------------------:|
|  Title   | `RESEARCH_PROBLEM`, `RESOURCE`, `PROCESS`, `LOCATION`, `METHOD`, `SOLUTION`, `TECHNOLOGY` |


### Approach
Based on the annotated dataset (findable under [`/data/processed`](data/processed)) we fine-tune the BERT pretrained model
using [`huggingface/transformers`](https://huggingface.co/docs/transformers/index) and define a token classification task.

For the implementation we mainly followed [this tutorial](https://huggingface.co/course/chapter7/2?fw=pt).

### Dataset

Raw dataset description can be found [here](https://github.com/jd-coderepos/contributions-ner-agri).
Under [`/data/processed`](data/processed) you can find the `IOB conll2003` input format of the dataset split into
`train`, `dev` and `test` files.


## How to run

### Prerequisites

#### Software Dependencies
* Python version ``^3.7.1``

#### Hardware Resources
These resources were provided on an instance of Google Colab and were partially used to fine-tune the BertForTokenClassification model.
* RAM ``~12 GB``
* Storage ``78 GB`` 
* Processor ``CPU``

### Cloning the repository

```commandline
git clone https://gitlab.com/TIBHannover/orkg/nlp/experiments/orkg-agriculture-ner.git
cd orkg-agriculture-ner
pip install -r requirements.txt
```

The repository includes the python script `src/main.py` which is a commandline tool responsible for doing all possible
tasks. The tool has the following syntax: 

```commandline
usage: main.py [-h] -t {predict} -mp MODEL_PATH -q QUERY
```

for full documentation, please type

```commandline
python -m src.main -h
```

### Service Retraining and Evaluation

Please use the notebook [`/notebooks/orkgnlp_agriculture_ner_training.ipynb`](notebooks/orkgnlp_agriculture_ner_training.ipynb)
for training, evaluating and saving the model. Follow the `TODO` instruction in the notebook for simple adjustments.

Note: For running the notebooks on Google Colab you will need to connect your Google Drive
account with the notebooks by providing your `MAIN_DRIVE_DIR` name.

### Service Integration

```commandline
python -m src.main -t predict -mp models/orkgnlp-agriculture-ner -q "your text"
```


## Contribution
This service is developed and maintained by:
* D'Souza, Jennifer <jennifer.dsouza@tib.eu>
* Arab-Oghli, Omar <omar.araboghli@tib.eu>

## License
[MIT](./LICENSE)


## References

* Subirats-Coll, Imma, et al. "[AGROVOC: The linked data concept hub for food and agriculture.](https://www.sciencedirect.com/science/article/pii/S0168169920331707)" Computers and Electronics in Agriculture 196 (2022): 105965.

